# SmartShoppingList

Program implementuje databázy, která umí 3 základní operace <br>
\+ nazev pocet -> přidá produkt a jeho počet do databáze <br>
\# -> vypíše od největšího po nejmenší všechny prdoukty a jejich počet<br>
? -> vypíše celkový počet prodaných produktů<br>
<br>
Ukazka běhu :<br>
Zadejte pozadavek :<br>
\+ jablko 3<br>
\+ mleko 5<br>
\+ chleba 4<br>
\#<br>
mleko 5<br>
chleba 4<br>
jablko 3<br>
?<br>
Celkove se prodalo 12<br>
\+ banan 14<br>
\+ jablko 5<br>
#<br>
banan 14<br>
jablko 8<br>
mleko 5<br>
chleba 4<br>