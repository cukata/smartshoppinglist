import java.util.Comparator;

public class CustomComparatorForAmount implements Comparator<Product> {
    @Override
    public int compare(Product o1, Product o2) {
        if( o1.amount < o2.amount  ) return 1;
        else if ( o1.amount > o2.amount) return -1;
        else return 0;
    }
}
