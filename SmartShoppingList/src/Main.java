import javax.swing.plaf.IconUIResource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        System.out.println("Zadejte pozadavek :");
        ArrayList<Product> products = new ArrayList<Product>();
        ArrayList<Product> nameOfProducts = new ArrayList<Product>();
        int totalSells = 0;

        Scanner sc = new Scanner( System.in );
        boolean endCondition = true;
        while (endCondition){
            String in = sc.nextLine();
            String [] arrOfIn = in.split(" ");

            switch ( arrOfIn[0] ){
                case "+" : { //adding

                    Product insertingProduct = new Product( arrOfIn[1], Integer.parseInt(arrOfIn[2]) );
                    int index = Collections.binarySearch( nameOfProducts, insertingProduct, new CustomComparatorForName() );

                    if( index < 0 ){
                        int productIndex = Collections.binarySearch( products, insertingProduct, new CustomComparatorForAmount() );
                        products.add( -1*(index + 1),insertingProduct );
                        nameOfProducts.add( -1*(index + 1) ,insertingProduct );
                        totalSells += Integer.parseInt(arrOfIn[2]);
                    }else {
                        Product newProduct = new Product(insertingProduct.name, insertingProduct.amount + nameOfProducts.get(index).amount);
                        int productIndex = Collections.binarySearch( products, new Product( insertingProduct.name, nameOfProducts.get(index).amount), new CustomComparatorForAmount() );
                        products.set( productIndex, newProduct );
                        nameOfProducts.set(index, newProduct );
                        totalSells += Integer.parseInt(arrOfIn[2]);
                    }

                }; break;
                case "#" : { //printing all sold product
                    Collections.sort( products, new CustomComparatorForAmount() );
                    for( int i = 0; i < products.size(); i++ ){
                        System.out.println( products.get(i).name + " " + products.get(i).amount );
                    }
                }break;
                case "?" : { //printing of total sum of sold product
                    System.out.println("Celkove se prodalo " + totalSells);
                }break;
                case "end":{
                    endCondition = false;
                }break;
                default:{
                    System.out.println("Toto neni validni operace");
                }
            }
        }
    }
}
